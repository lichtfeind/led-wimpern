EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L sk6812-ec15:sk6812-ec1515 U1
U 1 1 5D4DCA56
P 3100 2050
F 0 "U1" H 3100 2581 50  0000 C CNN
F 1 "sk6812-ec1515" H 3100 2490 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 3100 2050 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 3100 2050 50  0001 C CNN
	1    3100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR01
U 1 1 5D4DE7AC
P 3100 1400
F 0 "#PWR01" H 3100 1250 50  0001 C CNN
F 1 "+5V" H 3115 1573 50  0000 C CNN
F 2 "" H 3100 1400 50  0001 C CNN
F 3 "" H 3100 1400 50  0001 C CNN
	1    3100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2050 3750 2050
Wire Wire Line
	3100 1700 3100 1400
$Comp
L power:GND #PWR09
U 1 1 5D4DEE40
P 3100 2450
F 0 "#PWR09" H 3100 2200 50  0001 C CNN
F 1 "GND" H 3105 2277 50  0000 C CNN
F 2 "" H 3100 2450 50  0001 C CNN
F 3 "" H 3100 2450 50  0001 C CNN
	1    3100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 2450 3100 2400
$Comp
L sk6812-ec15:sk6812-ec1515 U2
U 1 1 5D4DF57D
P 4100 2050
F 0 "U2" H 4100 2581 50  0000 C CNN
F 1 "sk6812-ec1515" H 4100 2490 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 4100 2050 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 4100 2050 50  0001 C CNN
	1    4100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 5D4DF587
P 4100 1400
F 0 "#PWR02" H 4100 1250 50  0001 C CNN
F 1 "+5V" H 4115 1573 50  0000 C CNN
F 2 "" H 4100 1400 50  0001 C CNN
F 3 "" H 4100 1400 50  0001 C CNN
	1    4100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2050 4750 2050
Wire Wire Line
	4100 1700 4100 1400
$Comp
L power:GND #PWR010
U 1 1 5D4DF593
P 4100 2450
F 0 "#PWR010" H 4100 2200 50  0001 C CNN
F 1 "GND" H 4105 2277 50  0000 C CNN
F 2 "" H 4100 2450 50  0001 C CNN
F 3 "" H 4100 2450 50  0001 C CNN
	1    4100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2450 4100 2400
$Comp
L sk6812-ec15:sk6812-ec1515 U3
U 1 1 5D4E01D1
P 5100 2050
F 0 "U3" H 5100 2581 50  0000 C CNN
F 1 "sk6812-ec1515" H 5100 2490 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 5100 2050 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 5100 2050 50  0001 C CNN
	1    5100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR03
U 1 1 5D4E01DB
P 5100 1400
F 0 "#PWR03" H 5100 1250 50  0001 C CNN
F 1 "+5V" H 5115 1573 50  0000 C CNN
F 2 "" H 5100 1400 50  0001 C CNN
F 3 "" H 5100 1400 50  0001 C CNN
	1    5100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2050 5750 2050
Wire Wire Line
	5100 1700 5100 1400
$Comp
L power:GND #PWR011
U 1 1 5D4E01E7
P 5100 2450
F 0 "#PWR011" H 5100 2200 50  0001 C CNN
F 1 "GND" H 5105 2277 50  0000 C CNN
F 2 "" H 5100 2450 50  0001 C CNN
F 3 "" H 5100 2450 50  0001 C CNN
	1    5100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2450 5100 2400
$Comp
L sk6812-ec15:sk6812-ec1515 U4
U 1 1 5D4E1626
P 6100 2050
F 0 "U4" H 6100 2581 50  0000 C CNN
F 1 "sk6812-ec1515" H 6100 2490 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 6100 2050 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 6100 2050 50  0001 C CNN
	1    6100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5D4E1630
P 6100 1400
F 0 "#PWR04" H 6100 1250 50  0001 C CNN
F 1 "+5V" H 6115 1573 50  0000 C CNN
F 2 "" H 6100 1400 50  0001 C CNN
F 3 "" H 6100 1400 50  0001 C CNN
	1    6100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2050 6750 2050
Wire Wire Line
	6100 1700 6100 1400
$Comp
L power:GND #PWR012
U 1 1 5D4E163C
P 6100 2450
F 0 "#PWR012" H 6100 2200 50  0001 C CNN
F 1 "GND" H 6105 2277 50  0000 C CNN
F 2 "" H 6100 2450 50  0001 C CNN
F 3 "" H 6100 2450 50  0001 C CNN
	1    6100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 2450 6100 2400
$Comp
L sk6812-ec15:sk6812-ec1515 U5
U 1 1 5D4E27D3
P 7100 2050
F 0 "U5" H 7100 2581 50  0000 C CNN
F 1 "sk6812-ec1515" H 7100 2490 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 7100 2050 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 7100 2050 50  0001 C CNN
	1    7100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR05
U 1 1 5D4E27DD
P 7100 1400
F 0 "#PWR05" H 7100 1250 50  0001 C CNN
F 1 "+5V" H 7115 1573 50  0000 C CNN
F 2 "" H 7100 1400 50  0001 C CNN
F 3 "" H 7100 1400 50  0001 C CNN
	1    7100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 2050 7750 2050
Wire Wire Line
	7100 1700 7100 1400
$Comp
L power:GND #PWR013
U 1 1 5D4E27E9
P 7100 2450
F 0 "#PWR013" H 7100 2200 50  0001 C CNN
F 1 "GND" H 7105 2277 50  0000 C CNN
F 2 "" H 7100 2450 50  0001 C CNN
F 3 "" H 7100 2450 50  0001 C CNN
	1    7100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 2450 7100 2400
$Comp
L sk6812-ec15:sk6812-ec1515 U6
U 1 1 5D4E372D
P 8100 2050
F 0 "U6" H 8100 2581 50  0000 C CNN
F 1 "sk6812-ec1515" H 8100 2490 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 8100 2050 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 8100 2050 50  0001 C CNN
	1    8100 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR06
U 1 1 5D4E3737
P 8100 1400
F 0 "#PWR06" H 8100 1250 50  0001 C CNN
F 1 "+5V" H 8115 1573 50  0000 C CNN
F 2 "" H 8100 1400 50  0001 C CNN
F 3 "" H 8100 1400 50  0001 C CNN
	1    8100 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1700 8100 1400
$Comp
L power:GND #PWR014
U 1 1 5D4E3743
P 8100 2450
F 0 "#PWR014" H 8100 2200 50  0001 C CNN
F 1 "GND" H 8105 2277 50  0000 C CNN
F 2 "" H 8100 2450 50  0001 C CNN
F 3 "" H 8100 2450 50  0001 C CNN
	1    8100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2450 8100 2400
$Comp
L sk6812-ec15:sk6812-ec1515 U7
U 1 1 5D4F00BD
P 3100 3800
F 0 "U7" H 3100 4331 50  0000 C CNN
F 1 "sk6812-ec1515" H 3100 4240 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 3100 3800 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 3100 3800 50  0001 C CNN
	1    3100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR015
U 1 1 5D4F00C7
P 3100 3150
F 0 "#PWR015" H 3100 3000 50  0001 C CNN
F 1 "+5V" H 3115 3323 50  0000 C CNN
F 2 "" H 3100 3150 50  0001 C CNN
F 3 "" H 3100 3150 50  0001 C CNN
	1    3100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3800 3750 3800
Wire Wire Line
	3100 3450 3100 3150
$Comp
L power:GND #PWR021
U 1 1 5D4F00D3
P 3100 4200
F 0 "#PWR021" H 3100 3950 50  0001 C CNN
F 1 "GND" H 3105 4027 50  0000 C CNN
F 2 "" H 3100 4200 50  0001 C CNN
F 3 "" H 3100 4200 50  0001 C CNN
	1    3100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4200 3100 4150
$Comp
L sk6812-ec15:sk6812-ec1515 U8
U 1 1 5D4F00DE
P 4100 3800
F 0 "U8" H 4100 4331 50  0000 C CNN
F 1 "sk6812-ec1515" H 4100 4240 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 4100 3800 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 4100 3800 50  0001 C CNN
	1    4100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR016
U 1 1 5D4F00E8
P 4100 3150
F 0 "#PWR016" H 4100 3000 50  0001 C CNN
F 1 "+5V" H 4115 3323 50  0000 C CNN
F 2 "" H 4100 3150 50  0001 C CNN
F 3 "" H 4100 3150 50  0001 C CNN
	1    4100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3800 4750 3800
Wire Wire Line
	4100 3450 4100 3150
$Comp
L power:GND #PWR022
U 1 1 5D4F00F4
P 4100 4200
F 0 "#PWR022" H 4100 3950 50  0001 C CNN
F 1 "GND" H 4105 4027 50  0000 C CNN
F 2 "" H 4100 4200 50  0001 C CNN
F 3 "" H 4100 4200 50  0001 C CNN
	1    4100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 4200 4100 4150
$Comp
L sk6812-ec15:sk6812-ec1515 U9
U 1 1 5D4F00FF
P 5100 3800
F 0 "U9" H 5100 4331 50  0000 C CNN
F 1 "sk6812-ec1515" H 5100 4240 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 5100 3800 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 5100 3800 50  0001 C CNN
	1    5100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR017
U 1 1 5D4F0109
P 5100 3150
F 0 "#PWR017" H 5100 3000 50  0001 C CNN
F 1 "+5V" H 5115 3323 50  0000 C CNN
F 2 "" H 5100 3150 50  0001 C CNN
F 3 "" H 5100 3150 50  0001 C CNN
	1    5100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3800 5750 3800
Wire Wire Line
	5100 3450 5100 3150
$Comp
L power:GND #PWR023
U 1 1 5D4F0115
P 5100 4200
F 0 "#PWR023" H 5100 3950 50  0001 C CNN
F 1 "GND" H 5105 4027 50  0000 C CNN
F 2 "" H 5100 4200 50  0001 C CNN
F 3 "" H 5100 4200 50  0001 C CNN
	1    5100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 4200 5100 4150
$Comp
L sk6812-ec15:sk6812-ec1515 U10
U 1 1 5D4F0120
P 6100 3800
F 0 "U10" H 6100 4331 50  0000 C CNN
F 1 "sk6812-ec1515" H 6100 4240 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 6100 3800 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 6100 3800 50  0001 C CNN
	1    6100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR018
U 1 1 5D4F012A
P 6100 3150
F 0 "#PWR018" H 6100 3000 50  0001 C CNN
F 1 "+5V" H 6115 3323 50  0000 C CNN
F 2 "" H 6100 3150 50  0001 C CNN
F 3 "" H 6100 3150 50  0001 C CNN
	1    6100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 3800 6750 3800
Wire Wire Line
	6100 3450 6100 3150
$Comp
L power:GND #PWR024
U 1 1 5D4F0136
P 6100 4200
F 0 "#PWR024" H 6100 3950 50  0001 C CNN
F 1 "GND" H 6105 4027 50  0000 C CNN
F 2 "" H 6100 4200 50  0001 C CNN
F 3 "" H 6100 4200 50  0001 C CNN
	1    6100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 4200 6100 4150
$Comp
L sk6812-ec15:sk6812-ec1515 U11
U 1 1 5D4F0141
P 7100 3800
F 0 "U11" H 7100 4331 50  0000 C CNN
F 1 "sk6812-ec1515" H 7100 4240 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 7100 3800 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 7100 3800 50  0001 C CNN
	1    7100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR019
U 1 1 5D4F014B
P 7100 3150
F 0 "#PWR019" H 7100 3000 50  0001 C CNN
F 1 "+5V" H 7115 3323 50  0000 C CNN
F 2 "" H 7100 3150 50  0001 C CNN
F 3 "" H 7100 3150 50  0001 C CNN
	1    7100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3800 7750 3800
Wire Wire Line
	7100 3450 7100 3150
$Comp
L power:GND #PWR025
U 1 1 5D4F0157
P 7100 4200
F 0 "#PWR025" H 7100 3950 50  0001 C CNN
F 1 "GND" H 7105 4027 50  0000 C CNN
F 2 "" H 7100 4200 50  0001 C CNN
F 3 "" H 7100 4200 50  0001 C CNN
	1    7100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 4200 7100 4150
$Comp
L sk6812-ec15:sk6812-ec1515 U12
U 1 1 5D4F0162
P 8100 3800
F 0 "U12" H 8100 4331 50  0000 C CNN
F 1 "sk6812-ec1515" H 8100 4240 50  0000 C CNN
F 2 "sk6812_ec1515:sk6812_1515" H 8100 3800 50  0001 C CNN
F 3 "sk6812-ec15.pdf" H 8100 3800 50  0001 C CNN
	1    8100 3800
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR020
U 1 1 5D4F016C
P 8100 3150
F 0 "#PWR020" H 8100 3000 50  0001 C CNN
F 1 "+5V" H 8115 3323 50  0000 C CNN
F 2 "" H 8100 3150 50  0001 C CNN
F 3 "" H 8100 3150 50  0001 C CNN
	1    8100 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3450 8100 3150
$Comp
L power:GND #PWR026
U 1 1 5D4F0178
P 8100 4200
F 0 "#PWR026" H 8100 3950 50  0001 C CNN
F 1 "GND" H 8105 4027 50  0000 C CNN
F 2 "" H 8100 4200 50  0001 C CNN
F 3 "" H 8100 4200 50  0001 C CNN
	1    8100 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 4200 8100 4150
Wire Wire Line
	8450 2050 8700 2050
Wire Wire Line
	8700 2050 8700 2800
Wire Wire Line
	8700 2800 2550 2800
Wire Wire Line
	2550 2800 2550 3800
Wire Wire Line
	2550 3800 2750 3800
Wire Wire Line
	2750 2050 1850 2050
$Comp
L power:+5V #PWR07
U 1 1 5D4F61C4
P 1900 1850
F 0 "#PWR07" H 1900 1700 50  0001 C CNN
F 1 "+5V" H 1915 2023 50  0000 C CNN
F 2 "" H 1900 1850 50  0001 C CNN
F 3 "" H 1900 1850 50  0001 C CNN
	1    1900 1850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5D4F7660
P 1900 2250
F 0 "#PWR08" H 1900 2000 50  0001 C CNN
F 1 "GND" H 1905 2077 50  0000 C CNN
F 2 "" H 1900 2250 50  0001 C CNN
F 3 "" H 1900 2250 50  0001 C CNN
	1    1900 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 1850 1900 1950
Wire Wire Line
	1900 1950 1850 1950
Wire Wire Line
	1850 2150 1900 2150
Wire Wire Line
	1900 2150 1900 2250
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5E6A9381
P 1650 2050
F 0 "J1" H 1758 2331 50  0000 C CNN
F 1 "Conn_01x03_Male" H 1758 2240 50  0000 C CNN
F 2 "sk6812_ec1515:mini_con" H 1650 2050 50  0001 C CNN
F 3 "~" H 1650 2050 50  0001 C CNN
	1    1650 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5E6E76A0
P 3000 5700
F 0 "C1" H 3115 5746 50  0000 L CNN
F 1 "1u" H 3115 5655 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3038 5550 50  0001 C CNN
F 3 "~" H 3000 5700 50  0001 C CNN
	1    3000 5700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5E6E8A3A
P 3000 5850
F 0 "#PWR0101" H 3000 5600 50  0001 C CNN
F 1 "GND" H 3005 5677 50  0000 C CNN
F 2 "" H 3000 5850 50  0001 C CNN
F 3 "" H 3000 5850 50  0001 C CNN
	1    3000 5850
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0102
U 1 1 5E6E9637
P 3000 5550
F 0 "#PWR0102" H 3000 5400 50  0001 C CNN
F 1 "+5V" H 3015 5723 50  0000 C CNN
F 2 "" H 3000 5550 50  0001 C CNN
F 3 "" H 3000 5550 50  0001 C CNN
	1    3000 5550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
